/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bch.controller;

import cl.bch.dto.Respuesta;
import cl.bch.dto.Solicitud;
import cl.bch.dto.ResultadoServicio;
import cl.bch.servicios.ClientesPymeWS;
import cl.bch.servicios.ClientePersonasWS;
import cl.bch.utils.Base64Util;
import cl.bch.utils.PropertiesUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ImageMaker
 */
public class Controller {
    
    final static Logger LOGGER = LogManager.getLogger(Controller.class);
    
    Base64Util u64 = new Base64Util();
    PropertiesUtils uProp = new PropertiesUtils();
    
    /**
     * 
     * @param solicitud
     * @return
     * @throws Exception 
     */
    public Respuesta solicitarAdep(Solicitud solicitud) throws Exception{
        Respuesta resp = new Respuesta();
        ResultadoServicio rs;
        ClientesPymeWS cpyme = new ClientesPymeWS();
        ClientePersonasWS cper = new ClientePersonasWS();
        try{
            String wsdlStr = uProp.obtenerPropertie(Constantes.TOKENS_MAP.get(solicitud.getTokenProcess()));
            
            LOGGER.info("Consultando wsdl " + wsdlStr);
            
            if(codificarB64(solicitud.getTokenProcess())){
                solicitud.setStrEntrada(u64.textoABase64(solicitud.getStrEntrada()));
            }else{
                solicitud.setStrEntrada("<![CDATA[".concat(solicitud.getStrEntrada()).concat("]]>"));
            }
            
            if(tokenPersona(solicitud.getTokenProcess())){
                rs = cper.obtenerProductoGenerico(wsdlStr, solicitud.getStrEntrada());
            }else if (tokenPyme(solicitud.getTokenProcess())){
                if(Constantes.TOKENS.PYME_KIT_ORIGINALES.getToken().equals(solicitud.getTokenProcess())){
                    rs = cpyme.obtenerKitOriginal(wsdlStr, solicitud.getStrEntrada());
                } else if(Constantes.TOKENS.PYME_KIT_COPIAS.getToken().equals(solicitud.getTokenProcess())){
                    rs = cpyme.obtenerKitCopias(wsdlStr, solicitud.getStrEntrada());
                } else if(Constantes.TOKENS.PYME_PRODUCTOS_ORIGINALES.getToken().equals(solicitud.getTokenProcess())){
                    rs = cpyme.obtenerProductosOriginales(wsdlStr, solicitud.getStrEntrada());
                } else if(Constantes.TOKENS.PYME_PRODUCTOS_COPIAS.getToken().equals(solicitud.getTokenProcess())){
                    rs = cpyme.obtenerProductosCopias(wsdlStr, solicitud.getStrEntrada());
                } else {
                    rs = new ResultadoServicio();
                }
            }else{
                rs = cpyme.obtenerChecklist(wsdlStr, solicitud.getStrEntrada());
            }
            
            if(rs.getStrEncode() == null && rs.getStrResult() == null && rs.getStrReason() == null){
                resp.setBase64(null);
                resp.setCodigo(Constantes.CODIGOS.ERROR_CONSULTA.getCodigo());
                resp.setMotivo(Constantes.CODIGOS.ERROR_CONSULTA.getMotivo());
            }else{
                resp.setCodigo(rs.getStrResult() == null || rs.getStrResult().isEmpty() ? Constantes.CODIGOS.TERMINO_OK.getCodigo() : Integer.parseInt(rs.getStrResult()));
                resp.setMotivo(rs.getStrReason() == null || rs.getStrReason().isEmpty() ? Constantes.CODIGOS.TERMINO_OK.getMotivo() : rs.getStrReason());
                //Si hay un error adep Concat path (archivo no encontrado, se deja en null la respuesta ya que corresponde a la misma de entrada)
                resp.setBase64("0002".equals(rs.getStrResult()) ? null : rs.getStrEncode());
            }
            
        }catch(Exception e){
            resp.setBase64("");
            resp.setCodigo(Constantes.CODIGOS.TERMINO_NO_OK.getCodigo());
            resp.setMotivo(e.toString());
            throw e;
        }
        return resp;
    }
    
    /**
     * Metodo para diferenciar si el token corresponde a un process de personas
     * 
     * @param token
     * @return 
     */
    private boolean tokenPersona(String token){
        return token.startsWith("PER");
    }
    
    /**
     * Metodo para diferenciar si el token corresponde a un process de pyme
     * 
     * @param token
     * @return 
     */
    private boolean tokenPyme(String token){
        return token.startsWith("PYM");
    }

    /**
     * Metodo que retorna si por el token del proceso se debe codificar en base64
     * si el token no se encuentra retorna por defecto false
     * 
     * @param tokenProcess
     * @return 
     */
    private boolean codificarB64(String tokenProcess) {
        for(Constantes.TOKENS t : Constantes.TOKENS.values()){
            if(t.getToken().equals(tokenProcess)){
                return t.getB64();
            }
        }
        return false;
    }
}
