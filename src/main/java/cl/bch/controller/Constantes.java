/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bch.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ImageMaker
 */
public class Constantes {

    public static final String RUTA_PROPS = System.getProperty("APPS_PROPS") + "/" + "ConectorAdep_properties" + "/";
    
    public static final String APP_PROPERTIES = RUTA_PROPS + "app.properties";
    public static final String LOG4J_XML = RUTA_PROPS + "log4j2.xml";
    
    public static enum TOKENS {
        PYME_KIT_ORIGINALES("PYMKO", false), 
        PYME_KIT_COPIAS("PYMKC", false), 
        PYME_PRODUCTOS_ORIGINALES("PYMPO", false),
        PYME_PRODUCTOS_COPIAS("PYMPC", false),
        PERSONA_KIT_BANCO("PERKB", true),
        PERSONA_KIT_CLIENTE("PERKC", true),
        PERSONA_TARJETA_CREDITO("PERTC", true),
        PERSONA_CREDITO_CUOTAS("PERCC", true),
        PERSONA_CONSUMO_BANCO("PERCOO", true),
        PERSONA_CONSUMO_CLIENTE("PERCOC", true),
        PERSONA_CUENTA_VISTA("PERCVO", true),
        PERSONA_CUENTA_VISTA_COPIAS("PERCVC", true),
        PERSONA_CTA_CTE_MN("PERCMN", true),
        PERSONA_CTA_CTE_MX("PERCMX", true),
        CHECKLIST("CHECK", false);

        private final String token;
        private final boolean b64;

        TOKENS(String token, boolean b64) {
            this.token = token;
            this.b64 = b64;
        }

        public String getToken() {
            return token;
        }
        
        public boolean getB64() {
            return b64;
        }
    }
    
    public static final Map<String, String> TOKENS_MAP;
    static {
        Map<String, String> tokens = new HashMap<String, String>();
        tokens.put(TOKENS.PYME_KIT_ORIGINALES.getToken(), "wsdl.pyme.kit");
        tokens.put(TOKENS.PYME_KIT_COPIAS.getToken(), "wsdl.pyme.kit.copias");
        tokens.put(TOKENS.PYME_PRODUCTOS_ORIGINALES.getToken(), "wsdl.pyme.productos");
        tokens.put(TOKENS.PYME_PRODUCTOS_COPIAS.getToken(), "wsdl.pyme.productos.copias");
        
        tokens.put(TOKENS.PERSONA_KIT_BANCO.getToken(), "wsdl.persona.kit.banco");
        tokens.put(TOKENS.PERSONA_KIT_CLIENTE.getToken(), "wsdl.persona.kit.cliente");
        tokens.put(TOKENS.PERSONA_TARJETA_CREDITO.getToken(), "wsdl.persona.tarjeta.creito");
        tokens.put(TOKENS.PERSONA_CREDITO_CUOTAS.getToken(), "wsdl.persona.credito.cuotas");
        tokens.put(TOKENS.PERSONA_CONSUMO_BANCO.getToken(), "wsdl.persona.consumo.banco");
        tokens.put(TOKENS.PERSONA_CONSUMO_CLIENTE.getToken(), "wsdl.persona.consumo.cliente");
        tokens.put(TOKENS.PERSONA_CUENTA_VISTA.getToken(), "wsdl.persona.cuenta.vista");
        tokens.put(TOKENS.PERSONA_CUENTA_VISTA_COPIAS.getToken(), "wsdl.persona.cuenta.vista.copias");
        tokens.put(TOKENS.PERSONA_CTA_CTE_MN.getToken(), "wsdl.persona.cta.cte.mn");
        tokens.put(TOKENS.PERSONA_CTA_CTE_MX.getToken(), "wsdl.persona.cta.cte.mx");
        
        tokens.put("CHECK", "wsdl.checklist");
        TOKENS_MAP = Collections.unmodifiableMap(tokens);
    }
    
    public static enum CODIGOS {
        TERMINO_OK(0, "Término Exitoso"), 
        TERMINO_NO_OK(1, "Término No Exitoso"), 
        DATOS_INCORRECTOS(2, "Datos Incorrectos"),
        ERROR_CONSULTA(3, "Error al consultar Adep");

        private final int codigo;
        private final String motivo;

        CODIGOS(int codigo, String motivo) {
            this.codigo = codigo;
            this.motivo = motivo;
        }

        public int getCodigo() {
            return codigo;
        }

        public String getMotivo() {
            return motivo;
        }
    }
    
    public static final String PROP_CLAVES_PLATAFORMA = "conector.adep.usuarios.permitidos";
}
