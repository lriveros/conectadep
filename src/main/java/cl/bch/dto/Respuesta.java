/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bch.dto;

import java.io.Serializable;

/**
 *
 * @author lriveros
 */
public class Respuesta implements Serializable{
    private Integer codigo;
    private String motivo;
    private String base64;

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getBase64() {
        return base64;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getCodigo() {
        return codigo;
    }
}
