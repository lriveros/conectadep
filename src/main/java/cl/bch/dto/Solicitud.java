/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bch.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author lriveros
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="SolicitudType", propOrder={"keyProducto", "tokenProcess", "strEntrada"})
public class Solicitud implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @XmlElement(required=true)
    private String keyProducto;
    
    @XmlElement(required=true)
    private String tokenProcess;
    
    @XmlElement(required=true)
    private String strEntrada;

    public String getKeyProducto() {
        return keyProducto;
    }

    public String getTokenProcess() {
        return tokenProcess;
    }

    public String getStrEntrada() {
        return strEntrada;
    }

    public void setKeyProducto(String keyProducto) {
        this.keyProducto = keyProducto;
    }

    public void setTokenProcess(String tokenProcess) {
        this.tokenProcess = tokenProcess;
    }

    public void setStrEntrada(String strEntrada) {
        this.strEntrada = strEntrada;
    }
    
}
