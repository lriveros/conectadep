package cl.bch.services;

import cl.bch.dto.Respuesta;
import cl.bch.dto.Solicitud;
import cl.bch.controller.Constantes;
import cl.bch.controller.Constantes.CODIGOS;
import cl.bch.controller.Controller;
import cl.bch.utils.PropertiesUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

/**
 * Conector Adep Web Service
 *
 * @author lriveros
 */
@WebService(serviceName = "ConectAdepService")
public class ConectAdepService {
    
    final static Logger LOGGER = LogManager.getLogger(ConectAdepService.class);

    // use @Resource injection to create a WebServiceContext for server logging
    private @Resource
    WebServiceContext webServiceContext;
    
    PropertiesUtils pu = new PropertiesUtils();
    //Ip del cliente
    private String ipAddress;
    
    public ConectAdepService(){
        try {
            String log4jConfigFile = Constantes.LOG4J_XML;
            ConfigurationSource source = new ConfigurationSource(new FileInputStream(log4jConfigFile));
            Configurator.initialize(null, source);
        } catch (Exception ex) {
            LOGGER.error("No se puede cargar log4j", ex);
        }
    }
    
    @WebMethod(operationName = "obtenerPdf")
    public Respuesta obtenerPdf(@WebParam(name = "solicitud") Solicitud solicitud) {
        Respuesta resp = new Respuesta();
        Controller c = new Controller();
        try {
            HttpServletRequest request = getHttpServletRequest();
            
            ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }

            //Se valida token y clave
            if (validaSolicitud(solicitud)) {
                LOGGER.info("Request desde " + ipAddress + " - " + pu.obtenerNombrePlataforma(solicitud.getKeyProducto()) + " (Datos OK)");
                LOGGER.info("Parametros : " + solicitud.getKeyProducto() + " - " + solicitud.getTokenProcess() + " - " + previewString(solicitud.getStrEntrada()));
                LOGGER.trace("Request desde " + pu.obtenerNombrePlataforma(solicitud.getKeyProducto()) + " : " + solicitud.getStrEntrada());
                resp = c.solicitarAdep(solicitud);
                LOGGER.info("Response para " + ipAddress + " - " + previewString(resp.getBase64()));
                LOGGER.trace("Request transformado " + pu.obtenerNombrePlataforma(solicitud.getKeyProducto()) + " : " + solicitud.getStrEntrada());
                LOGGER.trace("Response para " + pu.obtenerNombrePlataforma(solicitud.getKeyProducto()) + " : " + resp.getBase64());
            } else {
                resp.setCodigo(CODIGOS.DATOS_INCORRECTOS.getCodigo());
                resp.setMotivo(CODIGOS.DATOS_INCORRECTOS.getMotivo());
            }
            
        } catch (Exception e) {
            LOGGER.error("Error al realizar la llamada del websevice", e);
            resp.setCodigo(CODIGOS.TERMINO_NO_OK.getCodigo());
            resp.setMotivo(CODIGOS.TERMINO_NO_OK.getMotivo());
        }
        return resp;
    }

    /**
     * Metodo que valida los datos de la solicitud
     *
     * @return
     */
    private boolean validaSolicitud(Solicitud solicitud) {
        
        if (Constantes.TOKENS_MAP.get(solicitud.getTokenProcess()) == null) {
            LOGGER.info(ipAddress + " | Realiza Solicitud con token invalido : " + solicitud.getTokenProcess());
            return false;
        }
        
        if (pu.obtenerNombrePlataforma(solicitud.getKeyProducto()).isEmpty()) {
            LOGGER.info(ipAddress + " | Realiza Solicitud con clave invalida : " + solicitud.getKeyProducto());
            return false;
        }
        return true;
    }

    /**
     * Retorna el preview de un String
     *
     * @param entrada
     * @return
     */
    private String previewString(String entrada) {
        int maxLargo = 30;
        if (entrada != null) {
            if (entrada.length() > maxLargo) {
                return entrada.replaceAll("\n", "").trim().substring(0, maxLargo).concat(" ... (" + entrada.length() + ")");
            } else {
                return entrada;
            }
        } else {
            return "";
        }
    }

    /**
     * Used for server logging. The operation is oneway: provides no response
     *
     * @param text
     *
     * @WebMethod(operationName = "log")
     * @Oneway public void logServer(@WebParam(name = "message") String text) {
     * // log message onto server getServletContext().log(text); }
    *
     */
    /**
     * Get ServletContext.
     *
     * @return ServletContext object
     */
    private ServletContext getServletContext() {
        return (ServletContext) webServiceContext.getMessageContext().get(
                MessageContext.SERVLET_CONTEXT);
    }

    /**
     * Get HttpServletRequest
     *
     * @return
     */
    private HttpServletRequest getHttpServletRequest() {
        return (HttpServletRequest) webServiceContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
    }
    
}
