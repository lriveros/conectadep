/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bch.utils;

import cl.bch.controller.Constantes;
import java.io.FileInputStream;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ImageMaker
 */
public class PropertiesUtils {
    
    final static Logger LOGGER = LogManager.getLogger(PropertiesUtils.class);
    
    private Properties props;
    
    /**
     * Metodo que carga el archivo properties
     * 
     * @throws Exception 
     */
    public void cargarArchivo() throws Exception {
        if (props == null) {
            props = new Properties();
            FileInputStream in = new FileInputStream(new java.io.File(Constantes.APP_PROPERTIES));
            props.load(in);
        }
    }
    
    /**
     * Metodo utilizado para obtener properties
     * 
     * @param key
     * @return
     * @throws Exception 
     */
    public String obtenerPropertie(String key) throws Exception {
        String resp;
        cargarArchivo();
        resp = props.getProperty(key);
        return resp;
    }
    
    /**
     * Busca en el archivo properties la plataforma a la que corresponde la clave ingresada
     * 
     * @param clave
     * @return 
     */
    public String obtenerNombrePlataforma(String clave) {
        String resp = "";
        try{
            String[] usuarios = obtenerPropertie(Constantes.PROP_CLAVES_PLATAFORMA).split(";");
            for (String u : usuarios) {
                String[] usuario = u.split(":");
                if(usuario[0].trim().equals(clave)){
                    resp = usuario[1];
                    break;
                }
            }
        }catch(Exception e){
            LOGGER.error("No se puede obtener listado de permisos desde properties", e);
            resp ="";
        }
        return resp;
    }
}
