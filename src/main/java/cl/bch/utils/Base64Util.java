/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.bch.utils;

import org.apache.commons.codec.binary.Base64;


/**
 *
 * @author ImageMaker
 */
public class Base64Util {
    
    public String textoABase64(String texto){
        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
        return new String(bytesEncoded);
    }
    
    public String base64ATexto(String base64){
        byte[] bytesEncoded = Base64.decodeBase64(base64);
        return new String(bytesEncoded);
    }
}
